import lejos.nxt.Motor;

public class Minigun {

	public static void main(String[] args) {
		Motor.A.setSpeed(360);
		Motor.C.setSpeed(360);
		while (true) {
			Motor.A.forward();
			Motor.C.forward();
			warten(100);
			//stop();
			Motor.A.backward();
			Motor.C.backward();
			warten(100);
			//stop();
		}
	}
	
	public static void warten(int timeInMillis) {
		try {
			Thread.sleep(timeInMillis);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void stop() {
		Motor.A.stop(true);
		Motor.C.stop();
	}
}
