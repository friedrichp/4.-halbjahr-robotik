import java.util.Random;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;

public class RoboterTest {

	public static void main(String[] args) {
		int currentMenuPoint = 1;
		String[] menuPointDesciption = {"fw", "rw", "fw + 90�", "Quadrat", "10 Sek. fw", "1m fw", "Kreis", "1m + Rndm-Turn + 1m"};
		LCD.drawString(currentMenuPoint + ". " + menuPointDesciption[currentMenuPoint-1], 0, 3);
		do {
			if (Button.LEFT.isPressed()) {
				if (currentMenuPoint > 1) {
					currentMenuPoint--;
					LCD.clear();
					LCD.drawString(currentMenuPoint + ". " + menuPointDesciption[currentMenuPoint-1], 0, 3);
					do {
						
					} while (Button.LEFT.isDown());
				}
			}
			if (Button.RIGHT.isPressed()) {
				if (currentMenuPoint < 8) {
					currentMenuPoint++;
					LCD.clear();
					LCD.drawString(currentMenuPoint + ". " + menuPointDesciption[currentMenuPoint-1], 0, 3);
					do {
						
					} while (Button.RIGHT.isDown());
				}
			}
		} while (!Button.ENTER.isPressed());
		
		if (currentMenuPoint == 1) {
			losfahren(360);
			warten(5000);
			stop();
		} else if (currentMenuPoint == 2) {
			losfahren(-360);
			warten(5000);
			stop();
		} else if (currentMenuPoint == 3) {
			losfahren(360);
			warten(5000);
			drehen(90);
			stop();
		} else if (currentMenuPoint == 4) {//Quadrat
			for (int i = 0; i < 4; i++) {
				losfahren(360);
				warten(2500);
				if (i == 2) {
					warten(200);
				}
				drehenRightHereRightNow(90);
			}
			losfahren(360);
			warten(100);
			stop();
		} else if (currentMenuPoint == 5) {
			losfahren(360);
			warten(10000);
			stop();
		} else if (currentMenuPoint == 6) {//1m
			losfahren(360);
			warten(5800);
		} else if (currentMenuPoint == 7) {//Kreis
			losfahren(360);
			drehen(360);
			stop();
		} else if (currentMenuPoint == 8) {//1m mit Random-Drehung
			losfahren(360);
			warten(5800);
			Random rando = new Random();
			if (rando.nextBoolean()) {
				drehen(90);
			} else drehen(-90);
			losfahren(360);
			warten(5800);
			stop();
		}
	}
	
	public static void losfahren(int speed) {
		Motor.A.setSpeed(speed);
		Motor.C.setSpeed(speed);
		if (speed < 0) {
			Motor.A.backward();
			Motor.C.backward();
		} else {
			Motor.A.forward();
			Motor.C.forward();
		}
		
	}
	
	public static void stop() {
		Motor.A.stop(true);
		Motor.C.stop();
	}
	
	public static void drehen(int gradNachRechts) {
		stop();
		if (gradNachRechts < 0) {
			Motor.A.setSpeed(300);
			Motor.C.setSpeed(180);
		} else {
			Motor.A.setSpeed(180);
			Motor.C.setSpeed(300);
		}
		Motor.A.forward();
		Motor.C.forward();
		warten(3240 * (Math.abs(gradNachRechts)/90));
	}
	
	public static void drehenRightHereRightNow(int gradNachRechts) {
		stop();
		Motor.A.setSpeed(360);
		Motor.C.setSpeed(360);
		if (gradNachRechts < 0) {
			Motor.A.forward();
			Motor.C.backward();
		} else {
			Motor.C.forward();
			Motor.A.backward();
		}
		warten(480 * (Math.abs(gradNachRechts)/90));
	}
	
	public static void warten(int timeInMillis) {
		try {
			Thread.sleep(timeInMillis);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
