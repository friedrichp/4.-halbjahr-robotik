import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;

public class BleibAufTisch {

	final static int MIN_LIGHT_VALUE_FOR_DRIVING = 38;
	static LightSensor lichtsensor = new LightSensor(SensorPort.S1);
	
	public static void main(String[] args) {
		boolean end = false;
		do {
			try {
				LCD.clear();
				LCD.drawString("Fahren", 0, 0);
				LCD.drawInt(lichtsensor.getLightValue(), 0, 1);
				fahreGeradeAus();
			} catch (Exception e) {
				stop();
				LCD.clear();
				LCD.drawString("Drehen", 0, 0);
				LCD.drawInt(lichtsensor.getLightValue(), 0, 1);
				Motor.A.setSpeed(360);
				Motor.C.setSpeed(360);
				Motor.A.backward();
				Motor.C.backward();
				warten(250);
				stop();
				drehenAberNichtWahrendessenGeradeausFahrenWeilDasWaehreNichtGutWeilDerRobiSonstVomTischFallenWuerde(45);
			}
		} while (end == false);
	}
	
	public static void fahreGeradeAus() throws Exception{
		Motor.A.setSpeed(360);
		Motor.C.setSpeed(360);
		Motor.A.forward();
		Motor.C.forward();
		for (int i = 0; i < 1000; i++) {
			warten(1);
			if (lichtsensor.getLightValue() < MIN_LIGHT_VALUE_FOR_DRIVING) {
				stop();
				throw new Exception("");
			}
		}
		stop();
		if (umschauen() == false) {
			stop();
			throw new Exception("");
		}
	}
	
	public static boolean umschauen() throws Exception {
		boolean isSafe = true;
		for (int i = 0; i < 5; i++) {
			drehenAberNichtWahrendessenGeradeausFahrenWeilDasWaehreNichtGutWeilDerRobiSonstVomTischFallenWuerde(5);
			if (lichtsensor.getLightValue() < MIN_LIGHT_VALUE_FOR_DRIVING) {
				stop();
				throw new Exception("");
			}
		}
		for (int i = 0; i < 1; i++) {
			drehenAberNichtWahrendessenGeradeausFahrenWeilDasWaehreNichtGutWeilDerRobiSonstVomTischFallenWuerde(-25);
		}
		for (int i = 0; i < 5; i++) {
			drehenAberNichtWahrendessenGeradeausFahrenWeilDasWaehreNichtGutWeilDerRobiSonstVomTischFallenWuerde(-5);
			if (lichtsensor.getLightValue() < MIN_LIGHT_VALUE_FOR_DRIVING) {
				stop();
				throw new Exception("");
			}
		}
		for (int i = 0; i < 1; i++) {
			drehenAberNichtWahrendessenGeradeausFahrenWeilDasWaehreNichtGutWeilDerRobiSonstVomTischFallenWuerde(25);
		}
		
		return isSafe;
	}
	
	public static void drehenAberNichtWahrendessenGeradeausFahrenWeilDasWaehreNichtGutWeilDerRobiSonstVomTischFallenWuerde(int gradNachRechts) {
		stop();
		Motor.A.setSpeed(360);
		Motor.C.setSpeed(360);
		if (gradNachRechts < 0) {
			Motor.A.forward();
			Motor.C.backward();
		} else {
			Motor.C.forward();
			Motor.A.backward();
		}
		warten(480 * Math.abs(gradNachRechts) / 90);
		stop();
	}
	
	public static void warten(int timeInMillis) {
		try {
			Thread.sleep(timeInMillis);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void stop() {
		Motor.A.stop(true);
		Motor.C.stop();
	}
	
	public static void minigun() {
		Motor.A.setSpeed(360);
		Motor.C.setSpeed(360);
		while (true) {
			Motor.A.forward();
			Motor.C.forward();
			Motor.A.backward();
			Motor.C.backward();
		}
	}
}
