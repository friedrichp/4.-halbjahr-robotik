import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;

public class FollowPath {

	static int searchLineWiederholungen = 0;
	static LightSensor lichtsensor = new LightSensor(SensorPort.S1);
	static final int NUMBER_OF_MENU_POINTS = 4;
	static final int START_VALUE_FOR_SEARCHLINEWIEDERHOLUNGEN = 6;
	static final int CONSTANT_SPEED = 90;
	static final int MAX_VAR_SPEED = 30;
	static final double CONSTANT_DREHUNG = 2.0;
	static final double MAX_VAR_DREHUNG = 1.5;
	static final double MAX_VALUE_FOR_DARK_GROUND = 39;

	public static void main(String[] args) {
		int currentMenuPoint = 1;
		String[] menuPointDesciption = {"simple\nFollowPath", "irgendein\nkomplizierter\nFollowPath-\nAlgorithmus\nder nicht\nrichtig\nfunktioniert", "Der komplizierte\nFollowPath-\nAlgorithmus\nnur in einfach.", "ultimate\nFollowPath"};
		LCD.drawString(currentMenuPoint + ". " + menuPointDesciption[currentMenuPoint-1], 0, 0);
		do {
			if (Button.LEFT.isPressed()) {
				if (currentMenuPoint > 1) {
					currentMenuPoint--;
					LCD.clear();
					LCD.drawString(currentMenuPoint + ". " + menuPointDesciption[currentMenuPoint-1], 0, 0);
					do {
						
					} while (Button.LEFT.isDown());
				}
			}
			if (Button.RIGHT.isPressed()) {
				if (currentMenuPoint < NUMBER_OF_MENU_POINTS) {
					currentMenuPoint++;
					LCD.clear();
					LCD.drawString(currentMenuPoint + ". " + menuPointDesciption[currentMenuPoint-1], 0, 0);
					do {
						
					} while (Button.RIGHT.isDown());
				}
			}
		} while (!Button.ENTER.isPressed());
		
		LCD.drawString("Egal", 0, 0);
		lichtsensor.setFloodlight(true);
		if (currentMenuPoint == 1) {
			simpleFollowPath();
		} else if (currentMenuPoint == 2) {
			irgendeinKomplizierterFollowPathAlgorithmusDerNichtRichtigFunktioniert();
		} else if (currentMenuPoint == 3) {
			derKomplizierteFollowPathAlgorithmusNurInEinfach();
		} else if (currentMenuPoint == 4) {
			ultimateFollowPath();
		}
		stop();
		Sound.beep();
	}
	
	public static void ultimateFollowPath() {
		Motor.A.setSpeed(360);
		Motor.C.setSpeed(1);
		Motor.A.forward();
		Motor.C.backward();
		do {
			if (isGroundDark()) {
				do {
					
				} while (isGroundDark());
				changeDirection();
			}
		} while (true);
		
	}
	
	public static void changeDirection() {
		int zs = Motor.A.getSpeed();
		Motor.A.setSpeed(Motor.C.getSpeed());
		Motor.C.setSpeed(zs);
		if (Motor.A.getSpeed()> Motor.C.getSpeed()) {
			Motor.A.forward();
			Motor.C.backward();
		} else {
			Motor.A.backward();
			Motor.C.forward();
		}
	}

	public static void derKomplizierteFollowPathAlgorithmusNurInEinfach() {
		Motor.A.setSpeed(360);
		Motor.C.setSpeed(360);
		do {
			if (isGroundDark()) {
				Motor.A.stop();
				Motor.C.forward();
			} else {
				Motor.C.stop();
				Motor.A.forward();
			}
		} while (true);
	}

	public static void simpleFollowPath() {
		boolean end = false;
		do {
			if (isGroundDark()) {
				Motor.A.forward();
				Motor.C.forward();
				Motor.A.setSpeed(360);
				Motor.C.setSpeed(360);
			} else {
				stop();
				searchLineWiederholungen = START_VALUE_FOR_SEARCHLINEWIEDERHOLUNGEN;
				if (searchLine(true) == false) {
					end = true;
				}
			}
		} while (!end);
	}
	
	public static boolean searchLine(boolean richtung) {
		searchLineWiederholungen++;
		if (searchLineWiederholungen > 90) {
			return false;
		} else {
			if (richtung) {
				drehenOnThisVeryPoint(searchLineWiederholungen);
				if (isGroundDark()) {
					return true;
				}
			} else {
				drehenOnThisVeryPoint(-searchLineWiederholungen);
				if (isGroundDark()) {
					return true;
				}
			}

			if (isGroundDark()) {
				return true;
			} else {
				if (richtung) {
					drehenOnThisVeryPoint(-searchLineWiederholungen);
				} else {
					drehenOnThisVeryPoint(searchLineWiederholungen);
				}
				if (isGroundDark()) {
					return true;
				} else return searchLine(!richtung);
			}
		}
	}
	
	public static void irgendeinKomplizierterFollowPathAlgorithmusDerNichtRichtigFunktioniert() {
		int ticsSinceLastDarkGround = 0;
		int ticsSinceLastWhiteGround = 0;
		int varSpeed = 0;
		double varDrehung = 0.0;
		if (isGroundDark()) {
			Motor.A.forward();
			Motor.C.forward();
			do {
				if (isGroundDark()) {
					ticsSinceLastDarkGround = 0;
					ticsSinceLastWhiteGround++;
					varSpeed = MAX_VAR_SPEED - (ticsSinceLastWhiteGround/5);
					if (varSpeed < 0) {
						varSpeed = 0;
					}
					varDrehung = MAX_VAR_DREHUNG - (ticsSinceLastWhiteGround/5000.0);
					if (varDrehung < 0) {
						varDrehung = 0;
					}
					Motor.A.setSpeed((int) ((CONSTANT_SPEED + varSpeed)*0.75));
					Motor.C.setSpeed((int) ((CONSTANT_SPEED + varSpeed)*(CONSTANT_DREHUNG + varDrehung)));
				} else {
					ticsSinceLastWhiteGround = 0;
					ticsSinceLastDarkGround++;
					LCD.clear();
					LCD.drawInt(ticsSinceLastDarkGround, 0, 0);
					varSpeed = MAX_VAR_SPEED - (ticsSinceLastDarkGround/10);
					if (varSpeed < 0) {
						varSpeed = 0;
					}
					varDrehung = MAX_VAR_DREHUNG - (ticsSinceLastDarkGround/5000.0);
					if (varDrehung < 0) {
						varDrehung = 0;
					}
					Motor.A.setSpeed((int) ((CONSTANT_SPEED + varSpeed)*(CONSTANT_DREHUNG + varDrehung)));
					Motor.C.setSpeed(CONSTANT_SPEED + varSpeed);
				}
			} while (true);
		} else {
			searchLineWiederholungen = START_VALUE_FOR_SEARCHLINEWIEDERHOLUNGEN;
			if (searchLine(true) == false) {
				stop();
			} else irgendeinKomplizierterFollowPathAlgorithmusDerNichtRichtigFunktioniert();
		}
	}

	public static void warten(int timeInMillis) {
		try {
			Thread.sleep(timeInMillis);
		} catch (InterruptedException e) {
			
		}
	}

	public static void stop() {
		Motor.A.stop(true);
		Motor.C.stop();
	}

	public static void drehenOnThisVeryPoint(int gradNachRechts) {
		stop();
		Motor.A.setSpeed(360);
		Motor.C.setSpeed(360);
		if (gradNachRechts < 0) {
			Motor.A.forward();
			Motor.C.backward();
		} else {
			Motor.C.forward();
			Motor.A.backward();
		}
		warten(480 * Math.abs(gradNachRechts) / 90);
		stop();
	}
	
	public static boolean isGroundDark() {
		if (lichtsensor.getLightValue() <= MAX_VALUE_FOR_DARK_GROUND) {
			return true;
		} else return false;
	}
}